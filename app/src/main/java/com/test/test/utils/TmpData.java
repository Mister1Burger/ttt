package com.test.test.utils;

import android.annotation.SuppressLint;

import com.test.test.adapter.PhotosAdapter;
import com.test.test.models.Photo;
import com.test.test.models.TouchImageView;

import java.util.ArrayList;
import java.util.List;

public class TmpData {
    public static final String BASE_URL = "https://api.unsplash.com/";
    public static final String ACCESS_KEY = "429080c6b591efce36044ba363fc175cb812af527e6bf196c8ccdcb1f30d0dfa";
    public static String SEARCHED_WORD = null;
    public static String keyWord;
    private Photo fullScreenPhoto = null;
    private PhotosAdapter photosAdapter = null;
    private int position = 0;
    private static TmpData instance;


    public static TmpData getInstance() {
        if (instance == null) {
            instance = new TmpData();
        }
        return instance;

    }

    public Photo getFullScreenPhoto() {
        return fullScreenPhoto;
    }

    public PhotosAdapter getPhotosAdapter() {
        return photosAdapter;
    }

    public int getPosition() {
        return position;
    }

    public void setFullScreenPhoto(Photo fullScreenPhoto) {
        this.fullScreenPhoto = fullScreenPhoto;
    }

    public void setPhotosAdapter(PhotosAdapter photosAdapter) {
        this.photosAdapter = photosAdapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
