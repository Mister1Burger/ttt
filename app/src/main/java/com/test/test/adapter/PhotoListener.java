package com.test.test.adapter;

import com.test.test.models.Photo;

public interface PhotoListener {
    void getPhoto(Photo photo);
}
