package com.test.test.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.test.test.R;
import com.test.test.R2;
import com.test.test.models.Photo;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.UserListViewHolder> implements Serializable {

    private List<Photo> photos;
    private PhotoListener listener;
    private Context context;


    public static class UserListViewHolder extends RecyclerView.ViewHolder implements Serializable {
        @BindView(R2.id.avatar)
        LinearLayout linearLayout;
        @BindView(R2.id.image_avatar)
        ImageView imageView;
        @BindView(R2.id.loading)
        TextView loading;



        UserListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public PhotosAdapter(List<Photo> photos, Context context, PhotoListener listener) {
        this.photos = photos;
        this.listener = listener;
        this.context = context;

    }

    private List<Photo> getPhotos() {
        return photos;
    }

    @NonNull
    @Override
    public PhotosAdapter.UserListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.avatar, parent, false);
        return new UserListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotosAdapter.UserListViewHolder holder, final int position) {
        holder.linearLayout.setOnClickListener(view -> listener.getPhoto(getPhotos().get(position)));
        Picasso.with(context).load(getPhotos().get(position).getUrls().getSmall()).transform(new RoundedTransformation(20, 0)).resize(800,600).centerCrop().into(holder.imageView, new Callback() {
            @Override
            public void onSuccess() {
                holder.loading.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError() {

            }
        });

        }

    @Override
    public int getItemCount() {
        return photos.size();
    }


    public void clear() {
        getPhotos().clear();
        notifyDataSetChanged();
    }

    public void addNewItems(List<Photo> newPhotos){
        for (int i = 0; i < newPhotos.size(); i++) {
            getPhotos().add(newPhotos.get(i));
        }
        notifyDataSetChanged();
    }

}
