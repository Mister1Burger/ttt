package com.test.test.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("urls")
        @Expose
        private Urls urls;

    public String getId() {
        return id;
    }

    public Urls getUrls() {
        return urls;
    }
}
