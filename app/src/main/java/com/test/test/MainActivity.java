package com.test.test;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.test.test.adapter.PhotosAdapter;
import com.test.test.models.Photo;
import com.test.test.models.SearchResult;
import com.test.test.models.TouchImageView;
import com.test.test.request.ResponseManager;
import com.test.test.utils.TmpData;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R2.id.search_et)
    EditText search_et;
    @BindView(R2.id.search_btn)
    ImageButton search_btn;
    @BindView(R2.id.list_photos)
    RecyclerView recyclerView;
    @BindView(R2.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R2.id.internet_trouble_layout)
    LinearLayout internetTroubleLayout;
    @BindView(R2.id.refresh_button)
    Button refresh_button;
    ImageButton downloadButton;


    private ResponseManager responseManager;
    private PhotosAdapter adapter;
    private Dialog dialog;
    private int lastVisibleItem, totalItemCount;


    private static boolean IS_SEARCHED = false;
    private static final int visibleThreshold = 5;
    private static int PAGE_TO_LOAD = 1;
    private GridLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        internetTroubleLayout.setVisibility(View.GONE);
        responseManager = new ResponseManager();
        responseManager.init();
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        search_btn.setOnClickListener(v -> {
            taskChooser();
            closeKeyboard();
        });
        refresh_button.setOnClickListener(v -> taskChooser());
        swipeRefreshLayout.setOnRefreshListener(this::taskChooser);
        search_et.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == 0) {
                taskChooser();
                closeKeyboard();
                return true;
            }
            return false;
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager
                        .findLastVisibleItemPosition();
                if (totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    PAGE_TO_LOAD++;
                    loadMore();
                }
            }
        });

        BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                if (referenceId != -1) {
                    Toast toast = Toast.makeText(MainActivity.this,
                            "Image Download Complete.Please, check your download folder", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(MainActivity.this,
                            "Image Download Failed", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 25, 400);
                    toast.show();
                }
                downloadButton.setEnabled(true);

            }
        };
        registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));


    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
        if(TmpData.getInstance().getPhotosAdapter() != null){
            adapter = TmpData.getInstance().getPhotosAdapter();
        }}catch (Exception e){
            }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        if(TmpData.getInstance().getFullScreenPhoto() != null){
            openFullScreenImage(TmpData.getInstance().getFullScreenPhoto());
        }
        
        View view = getCurrentFocus();

        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(INPUT_METHOD_SERVICE)))
                    .restartInput(view);
        }
        taskChooser();
        }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            TmpData.getInstance().setPhotosAdapter(adapter);
            TmpData.getInstance().setPosition(layoutManager.findLastVisibleItemPosition());
        }catch (Exception e){}
    }

    private void getPhotos(String keyWord, int pagesNumber) {
        swipeRefreshLayout.setRefreshing(true);
        if (keyWord.equals("")) {
            responseManager.getPhotos(pagesNumber, 20, new ResponseManager.OnPhotosLoadedListener() {
                @Override
                public void onComplete(List<Photo> photos) {
                    if (adapter != null) {
                        if (IS_SEARCHED) {
                            adapter.clear();
                            adapter.addNewItems(photos);
                            recyclerView.scrollToPosition(1);
                            IS_SEARCHED = false;
                        }else if(TmpData.getInstance().getPosition() != 0){
                            Log.d("TEG","POSITION " + String.valueOf(TmpData.getInstance().getPosition()));
                            recyclerView.setAdapter(adapter);
                            recyclerView.scrollToPosition(TmpData.getInstance().getPosition());
                        }
                    } else {
                        Log.d("TEG", String.valueOf(photos.size()));
                        adapter = new PhotosAdapter(photos, getBaseContext(), photo -> openFullScreenImage(photo));
                        recyclerView.setAdapter(adapter);
                    }
                    internetTroubleLayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onError(String error) {
                    Log.d("TAG", error);
                    internetTroubleLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            responseManager.searchPhotos(keyWord, pagesNumber, 20, new ResponseManager.OnSearchCompleteListener() {
                @Override
                public void onComplete(SearchResult results) {
                    if (!IS_SEARCHED) {
                        adapter.clear();
                        adapter.addNewItems(results.getResults());
                        recyclerView.scrollToPosition(1);
                        IS_SEARCHED = true;
                    }else if(TmpData.getInstance().getPosition() != 0){
                        Log.d("TEG","POSITION " + String.valueOf(TmpData.getInstance().getPosition()));
                        recyclerView.setAdapter(adapter);
                        recyclerView.scrollToPosition(TmpData.getInstance().getPosition());
                    }
                    internetTroubleLayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onError(String error) {
                    internetTroubleLayout.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

    }


    private void openFullScreenImage(Photo photo) {

        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.full_screen_image);
        ProgressBar spinner = dialog.findViewById(R.id.spinner);
        if (photo != null) {
            TouchImageView image = dialog.findViewById(R.id.full_screen);
            Picasso.with(this).load(photo.getUrls().getRegular()).into(image, new Callback() {
                @Override
                public void onSuccess() {
                    spinner.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    Toast toast = Toast.makeText(MainActivity.this,
                            "Cannot load the image", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                    return;
                }
            });
            ImageButton closeButton = dialog.findViewById(R.id.close);
            closeButton.setOnClickListener((V) -> {dialog.dismiss();TmpData.getInstance().setFullScreenPhoto(null);});



            downloadButton = dialog.findViewById(R.id.download);
            downloadButton.setOnClickListener((V) -> {
                downloadButton.setEnabled(false);
                Toast toast = Toast.makeText(MainActivity.this,
                        "Starting Download", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
                downloadImage(photo.getUrls().getFull());
            });
            TmpData.getInstance().setFullScreenPhoto(photo);
            dialog.setCancelable(false);
            dialog.show();
        }

    }

    private void taskChooser() {
        TmpData.keyWord = search_et.getText().toString();
        Log.d("TEG", "TASK " +String.valueOf(TmpData.keyWord )+ "  " + String.valueOf(TmpData.SEARCHED_WORD));
        if (TmpData.keyWord.equals("") && TmpData.SEARCHED_WORD != null) {
            PAGE_TO_LOAD = 1;
            getPhotos(TmpData.keyWord, PAGE_TO_LOAD);
            TmpData.SEARCHED_WORD = null;
        } else if (!TmpData.keyWord.equals("") && !TmpData.keyWord.equals(TmpData.SEARCHED_WORD)) {
            PAGE_TO_LOAD = 1;
            getPhotos(TmpData.keyWord, PAGE_TO_LOAD);
            TmpData.SEARCHED_WORD = TmpData.keyWord;
        } else {
            getPhotos(TmpData.keyWord, PAGE_TO_LOAD);
        }
    }

    private void loadMore() {
        swipeRefreshLayout.setRefreshing(true);
        if (TmpData.keyWord != null && !TmpData.keyWord.equals("")) {
            responseManager.searchPhotos(TmpData.keyWord, PAGE_TO_LOAD, 20, new ResponseManager.OnSearchCompleteListener() {
                @Override
                public void onComplete(SearchResult results) {
                    adapter.addNewItems(results.getResults());
                    refresh_button.setVisibility(View.VISIBLE);
                    internetTroubleLayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onError(String error) {
                    internetTroubleLayout.setVisibility(View.VISIBLE);
                    refresh_button.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        } else {
            responseManager.getPhotos(PAGE_TO_LOAD, 20, new ResponseManager.OnPhotosLoadedListener() {
                @Override
                public void onComplete(List<Photo> photos) {
                    adapter.addNewItems(photos);
                    refresh_button.setVisibility(View.VISIBLE);
                    internetTroubleLayout.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onError(String error) {
                    internetTroubleLayout.setVisibility(View.VISIBLE);
                    refresh_button.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

    }

    private long downloadImage(String url) {
        long downloadReference;

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        DateFormat dateFormat = new SimpleDateFormat("MM_dd_HH_mm_ss");
        Date date = new Date();

        request.setTitle("Download");
        request.setDescription("please wait");

        request.setDestinationInExternalFilesDir(MainActivity.this,
                Environment.DIRECTORY_DOWNLOADS, "PHOTO_" + dateFormat.format(date) + ".jpg");

        assert downloadManager != null;
        downloadReference = downloadManager.enqueue(request);


        return downloadReference;
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}


