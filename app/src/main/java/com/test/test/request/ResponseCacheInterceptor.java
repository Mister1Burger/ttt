package com.test.test.request;
import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ResponseCacheInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        request = request.newBuilder()
                .addHeader("Authorization", "Client-ID " + "429080c6b591efce36044ba363fc175cb812af527e6bf196c8ccdcb1f30d0dfa")
                .build();
        return chain.proceed(request);
    }
}
