package com.test.test.request;

import com.test.test.models.Photo;
import com.test.test.models.SearchResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ResponseInterface {

    @GET("photos")
    Call<List<Photo>> getPhotos(@Query("client_id") String t,@Query("page") Integer page, @Query("per_page") Integer perPage);


    @GET("search/photos")
    Call<SearchResult> searchPhotos(@Query("client_id") String t,@Query("query") String query, @Query("page") Integer page, @Query("per_page") Integer perPage);
}
