package com.test.test.request;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.test.test.models.Photo;
import com.test.test.models.SearchResult;
import com.test.test.utils.TmpData;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResponseManager {
    private ResponseInterface responseInterface;

    public void init() {

        OkHttpClient client = new OkHttpClient.Builder()

                .addInterceptor(new ResponseCacheInterceptor()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TmpData.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        responseInterface = retrofit.create(ResponseInterface.class);
    }


    public void getPhotos(Integer page, Integer perPage, final OnPhotosLoadedListener listener){
        Call<List<Photo>> call = responseInterface.getPhotos(TmpData.ACCESS_KEY, page,perPage);
        call.enqueue(getMultiplePhotoCallback(listener));
    }

    public void searchPhotos(@NonNull String query, OnSearchCompleteListener listener) {
        searchPhotos(query, null, null, listener);
    }

    public void searchPhotos(@NonNull String query, @Nullable Integer page, @Nullable Integer perPage, OnSearchCompleteListener listener) {
        Call<SearchResult> call = responseInterface.searchPhotos(TmpData.ACCESS_KEY,query, page, perPage);
        call.enqueue(getSearchResultsCallback(listener));
    }

    private Callback<SearchResult> getSearchResultsCallback(final OnSearchCompleteListener listener) {
        return new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                int statusCode = response.code();
                Log.d("TAG", "Status Code = " + statusCode);
                if (statusCode == 200) {
                    SearchResult results = response.body();
                    listener.onComplete(results);
                } else if (statusCode == 401) {
                    Log.d("TAG", "Unauthorized, Check your client Id");
                }
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        };
    }
    private Callback<List<Photo>> getMultiplePhotoCallback(final OnPhotosLoadedListener listener){
        return new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
                int statusCode = response.code();
                Log.d("TAG", "Url = " + call.request().url());
                Log.d("TAG", "Status Code = " + statusCode);
                if(statusCode == 200) {
                    List<Photo> photos = response.body();
                    listener.onComplete(photos);
                }
                else if(statusCode == 401) {
                    Log.d("TAG", "Unauthorized, Check your client Id");
                }
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                Log.d("TAG", "Url = " + call.request().url());
                listener.onError(t.getMessage());
            }
        };
    }


    public interface OnPhotosLoadedListener {
        void onComplete(List<Photo> photos);

        void onError(String error);
    }

    public interface OnSearchCompleteListener {
        void onComplete(SearchResult results);

        void onError(String error);
    }

}
